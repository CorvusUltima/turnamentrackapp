﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrackerLibrary;

namespace TracerUI
{
    public partial class CreatePrizeForm : Form
    {
        public CreatePrizeForm()
        {
            InitializeComponent();
        }

        private void FrstNameLabel_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void OrLabel_Click(object sender, EventArgs e)
        {

        }

       private void CreatePrizeButton_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
            {
                PrizeModel model = new PrizeModel
                    (PlaceNumberValue.Text,
                    PlaceNameValue.Text,
                    PrizeAmountValue.Text, 
                    PrizePercentageValue.Text);
                foreach(IDataConnection db in GlobalConfig.Connections)
                {
                    db.CreatePrize(model);
                }
            }
            else
            {
                MessageBox.Show("This form has invalid information");
            }
        }
        private bool ValidateForm()
        {

            bool output = true;
            int placeNumber = 0;
            bool placeNumberValidNumber = int.TryParse(PlaceNumberValue.Text, out placeNumber);
               if(!placeNumberValidNumber)
            {

                output = false;
            }
               if(placeNumber<1)
            {

                output = false;
            }
            
               if(PlaceNameValue.Text.Length == 0)
            {

                output = false;
            }

            decimal prizeAmount = 0;
            double prizePercentage = 0;

            bool prizeNumberValidNumber = decimal.TryParse(PrizeAmountValue.Text, out prizeAmount);
            bool prizePercentageValidNumber = double.TryParse(PrizePercentageValue.Text, out prizePercentage);

            if(!prizeNumberValidNumber || !prizePercentageValidNumber)
            {
                output = false;
            }
            if(prizeAmount<=0 && prizePercentage<=0)
            {
                output = false;
            }

            if(prizePercentage< 0 || prizePercentage>100)
            {
                output = false;
            }

            return output;

        }

    }
}

﻿
namespace TracerUI
{
    partial class CreateTeamForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CreateTeamLabel = new System.Windows.Forms.Label();
            this.TeamNameValue = new System.Windows.Forms.TextBox();
            this.TeamNameLabel = new System.Windows.Forms.Label();
            this.AddMemberButton = new System.Windows.Forms.Button();
            this.SelectTeamMemberDropDown = new System.Windows.Forms.ComboBox();
            this.SelectTeamMemberlabel = new System.Windows.Forms.Label();
            this.addNewMemberBox = new System.Windows.Forms.GroupBox();
            this.FrstNameValue = new System.Windows.Forms.TextBox();
            this.FrstNameLabel = new System.Windows.Forms.Label();
            this.LastNameValue = new System.Windows.Forms.TextBox();
            this.LastNameLabel = new System.Windows.Forms.Label();
            this.EmailValue = new System.Windows.Forms.TextBox();
            this.EmailLabel = new System.Windows.Forms.Label();
            this.CellPhoneValue = new System.Windows.Forms.TextBox();
            this.CellPhoneLabel = new System.Windows.Forms.Label();
            this.CreateMemberButton = new System.Windows.Forms.Button();
            this.TeamMembersListBox = new System.Windows.Forms.ListBox();
            this.DeleteSelectedTeamMemberButton = new System.Windows.Forms.Button();
            this.CreateTeamButton = new System.Windows.Forms.Button();
            this.addNewMemberBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // CreateTeamLabel
            // 
            this.CreateTeamLabel.AutoSize = true;
            this.CreateTeamLabel.Font = new System.Drawing.Font("Segoe UI Light", 22F);
            this.CreateTeamLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.CreateTeamLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.CreateTeamLabel.Location = new System.Drawing.Point(350, 9);
            this.CreateTeamLabel.Name = "CreateTeamLabel";
            this.CreateTeamLabel.Size = new System.Drawing.Size(252, 60);
            this.CreateTeamLabel.TabIndex = 26;
            this.CreateTeamLabel.Text = "Create Team";
            this.CreateTeamLabel.Click += new System.EventHandler(this.TeamNameLabel_Click);
            // 
            // TeamNameValue
            // 
            this.TeamNameValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TeamNameValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(151)))), ((int)(((byte)(255)))));
            this.TeamNameValue.Location = new System.Drawing.Point(21, 75);
            this.TeamNameValue.Name = "TeamNameValue";
            this.TeamNameValue.Size = new System.Drawing.Size(391, 71);
            this.TeamNameValue.TabIndex = 29;
            // 
            // TeamNameLabel
            // 
            this.TeamNameLabel.AutoSize = true;
            this.TeamNameLabel.Font = new System.Drawing.Font("Segoe UI", 20F);
            this.TeamNameLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.TeamNameLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.TeamNameLabel.Location = new System.Drawing.Point(12, 18);
            this.TeamNameLabel.Name = "TeamNameLabel";
            this.TeamNameLabel.Size = new System.Drawing.Size(231, 54);
            this.TeamNameLabel.TabIndex = 30;
            this.TeamNameLabel.Text = "Team Name";
            // 
            // AddMemberButton
            // 
            this.AddMemberButton.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.AddMemberButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.AddMemberButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.AddMemberButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddMemberButton.Font = new System.Drawing.Font("Segoe UI Semibold", 16F, System.Drawing.FontStyle.Bold);
            this.AddMemberButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.AddMemberButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.AddMemberButton.Location = new System.Drawing.Point(81, 288);
            this.AddMemberButton.Name = "AddMemberButton";
            this.AddMemberButton.Size = new System.Drawing.Size(222, 57);
            this.AddMemberButton.TabIndex = 33;
            this.AddMemberButton.Text = "Add Member";
            this.AddMemberButton.UseVisualStyleBackColor = true;
            // 
            // SelectTeamMemberDropDown
            // 
            this.SelectTeamMemberDropDown.FormattingEnabled = true;
            this.SelectTeamMemberDropDown.Location = new System.Drawing.Point(21, 209);
            this.SelectTeamMemberDropDown.Name = "SelectTeamMemberDropDown";
            this.SelectTeamMemberDropDown.Size = new System.Drawing.Size(391, 73);
            this.SelectTeamMemberDropDown.TabIndex = 32;
            // 
            // SelectTeamMemberlabel
            // 
            this.SelectTeamMemberlabel.AutoSize = true;
            this.SelectTeamMemberlabel.Font = new System.Drawing.Font("Segoe UI", 20F);
            this.SelectTeamMemberlabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.SelectTeamMemberlabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SelectTeamMemberlabel.Location = new System.Drawing.Point(12, 149);
            this.SelectTeamMemberlabel.Name = "SelectTeamMemberlabel";
            this.SelectTeamMemberlabel.Size = new System.Drawing.Size(392, 54);
            this.SelectTeamMemberlabel.TabIndex = 31;
            this.SelectTeamMemberlabel.Text = "Select Team Member";
            this.SelectTeamMemberlabel.Click += new System.EventHandler(this.SelectTeamLabel_Click);
            // 
            // addNewMemberBox
            // 
            this.addNewMemberBox.Controls.Add(this.CreateMemberButton);
            this.addNewMemberBox.Controls.Add(this.CellPhoneValue);
            this.addNewMemberBox.Controls.Add(this.CellPhoneLabel);
            this.addNewMemberBox.Controls.Add(this.EmailValue);
            this.addNewMemberBox.Controls.Add(this.EmailLabel);
            this.addNewMemberBox.Controls.Add(this.LastNameValue);
            this.addNewMemberBox.Controls.Add(this.LastNameLabel);
            this.addNewMemberBox.Controls.Add(this.FrstNameValue);
            this.addNewMemberBox.Controls.Add(this.FrstNameLabel);
            this.addNewMemberBox.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.addNewMemberBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.addNewMemberBox.Location = new System.Drawing.Point(8, 357);
            this.addNewMemberBox.Name = "addNewMemberBox";
            this.addNewMemberBox.Size = new System.Drawing.Size(415, 330);
            this.addNewMemberBox.TabIndex = 34;
            this.addNewMemberBox.TabStop = false;
            this.addNewMemberBox.Text = "Add new member";
            this.addNewMemberBox.Enter += new System.EventHandler(this.addNewMemberBox_Enter);
            // 
            // FrstNameValue
            // 
            this.FrstNameValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FrstNameValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(151)))), ((int)(((byte)(255)))));
            this.FrstNameValue.Location = new System.Drawing.Point(194, 46);
            this.FrstNameValue.Name = "FrstNameValue";
            this.FrstNameValue.Size = new System.Drawing.Size(209, 50);
            this.FrstNameValue.TabIndex = 12;
            // 
            // FrstNameLabel
            // 
            this.FrstNameLabel.AutoSize = true;
            this.FrstNameLabel.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.FrstNameLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.FrstNameLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.FrstNameLabel.Location = new System.Drawing.Point(-5, 46);
            this.FrstNameLabel.Name = "FrstNameLabel";
            this.FrstNameLabel.Size = new System.Drawing.Size(166, 45);
            this.FrstNameLabel.TabIndex = 11;
            this.FrstNameLabel.Text = "Frst Name";
            // 
            // LastNameValue
            // 
            this.LastNameValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LastNameValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(151)))), ((int)(((byte)(255)))));
            this.LastNameValue.Location = new System.Drawing.Point(195, 101);
            this.LastNameValue.Name = "LastNameValue";
            this.LastNameValue.Size = new System.Drawing.Size(209, 50);
            this.LastNameValue.TabIndex = 14;
            // 
            // LastNameLabel
            // 
            this.LastNameLabel.AutoSize = true;
            this.LastNameLabel.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.LastNameLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.LastNameLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LastNameLabel.Location = new System.Drawing.Point(-4, 101);
            this.LastNameLabel.Name = "LastNameLabel";
            this.LastNameLabel.Size = new System.Drawing.Size(170, 45);
            this.LastNameLabel.TabIndex = 13;
            this.LastNameLabel.Text = "Last Name";
            // 
            // EmailValue
            // 
            this.EmailValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.EmailValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(151)))), ((int)(((byte)(255)))));
            this.EmailValue.Location = new System.Drawing.Point(194, 155);
            this.EmailValue.Name = "EmailValue";
            this.EmailValue.Size = new System.Drawing.Size(209, 50);
            this.EmailValue.TabIndex = 16;
            // 
            // EmailLabel
            // 
            this.EmailLabel.AutoSize = true;
            this.EmailLabel.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.EmailLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.EmailLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.EmailLabel.Location = new System.Drawing.Point(-5, 155);
            this.EmailLabel.Name = "EmailLabel";
            this.EmailLabel.Size = new System.Drawing.Size(96, 45);
            this.EmailLabel.TabIndex = 15;
            this.EmailLabel.Text = "Email";
            // 
            // CellPhoneValue
            // 
            this.CellPhoneValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CellPhoneValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(151)))), ((int)(((byte)(255)))));
            this.CellPhoneValue.Location = new System.Drawing.Point(195, 211);
            this.CellPhoneValue.Name = "CellPhoneValue";
            this.CellPhoneValue.Size = new System.Drawing.Size(209, 50);
            this.CellPhoneValue.TabIndex = 18;
            // 
            // CellPhoneLabel
            // 
            this.CellPhoneLabel.AutoSize = true;
            this.CellPhoneLabel.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.CellPhoneLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.CellPhoneLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.CellPhoneLabel.Location = new System.Drawing.Point(-4, 211);
            this.CellPhoneLabel.Name = "CellPhoneLabel";
            this.CellPhoneLabel.Size = new System.Drawing.Size(163, 45);
            this.CellPhoneLabel.TabIndex = 17;
            this.CellPhoneLabel.Text = "CellPhone";
            // 
            // CreateMemberButton
            // 
            this.CreateMemberButton.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.CreateMemberButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.CreateMemberButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.CreateMemberButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateMemberButton.Font = new System.Drawing.Font("Segoe UI Semibold", 16F, System.Drawing.FontStyle.Bold);
            this.CreateMemberButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.CreateMemberButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.CreateMemberButton.Location = new System.Drawing.Point(73, 267);
            this.CreateMemberButton.Name = "CreateMemberButton";
            this.CreateMemberButton.Size = new System.Drawing.Size(290, 57);
            this.CreateMemberButton.TabIndex = 34;
            this.CreateMemberButton.Text = "Create Member";
            this.CreateMemberButton.UseVisualStyleBackColor = true;
            // 
            // TeamMembersListBox
            // 
            this.TeamMembersListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TeamMembersListBox.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.TeamMembersListBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(151)))), ((int)(((byte)(255)))));
            this.TeamMembersListBox.FormattingEnabled = true;
            this.TeamMembersListBox.ItemHeight = 32;
            this.TeamMembersListBox.Location = new System.Drawing.Point(445, 75);
            this.TeamMembersListBox.Name = "TeamMembersListBox";
            this.TeamMembersListBox.Size = new System.Drawing.Size(282, 610);
            this.TeamMembersListBox.TabIndex = 35;
            // 
            // DeleteSelectedTeamMemberButton
            // 
            this.DeleteSelectedTeamMemberButton.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.DeleteSelectedTeamMemberButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.DeleteSelectedTeamMemberButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.DeleteSelectedTeamMemberButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteSelectedTeamMemberButton.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.DeleteSelectedTeamMemberButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.DeleteSelectedTeamMemberButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.DeleteSelectedTeamMemberButton.Location = new System.Drawing.Point(733, 283);
            this.DeleteSelectedTeamMemberButton.Name = "DeleteSelectedTeamMemberButton";
            this.DeleteSelectedTeamMemberButton.Size = new System.Drawing.Size(131, 77);
            this.DeleteSelectedTeamMemberButton.TabIndex = 36;
            this.DeleteSelectedTeamMemberButton.Text = "Delete Selected";
            this.DeleteSelectedTeamMemberButton.UseVisualStyleBackColor = true;
            // 
            // CreateTeamButton
            // 
            this.CreateTeamButton.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.CreateTeamButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.CreateTeamButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.CreateTeamButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateTeamButton.Font = new System.Drawing.Font("Segoe UI Semibold", 16F, System.Drawing.FontStyle.Bold);
            this.CreateTeamButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.CreateTeamButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.CreateTeamButton.Location = new System.Drawing.Point(202, 694);
            this.CreateTeamButton.Name = "CreateTeamButton";
            this.CreateTeamButton.Size = new System.Drawing.Size(346, 57);
            this.CreateTeamButton.TabIndex = 37;
            this.CreateTeamButton.Text = "Create Team";
            this.CreateTeamButton.UseVisualStyleBackColor = true;
            // 
            // CreateTeamForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(27F, 65F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(890, 763);
            this.Controls.Add(this.CreateTeamButton);
            this.Controls.Add(this.DeleteSelectedTeamMemberButton);
            this.Controls.Add(this.TeamMembersListBox);
            this.Controls.Add(this.addNewMemberBox);
            this.Controls.Add(this.AddMemberButton);
            this.Controls.Add(this.SelectTeamMemberDropDown);
            this.Controls.Add(this.SelectTeamMemberlabel);
            this.Controls.Add(this.TeamNameLabel);
            this.Controls.Add(this.TeamNameValue);
            this.Controls.Add(this.CreateTeamLabel);
            this.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.Margin = new System.Windows.Forms.Padding(14, 13, 14, 13);
            this.Name = "CreateTeamForm";
            this.Text = "Create Team ";
            this.addNewMemberBox.ResumeLayout(false);
            this.addNewMemberBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label CreateTeamLabel;
        private System.Windows.Forms.TextBox TeamNameValue;
        private System.Windows.Forms.Label TeamNameLabel;
        private System.Windows.Forms.Button AddMemberButton;
        private System.Windows.Forms.ComboBox SelectTeamMemberDropDown;
        private System.Windows.Forms.Label SelectTeamMemberlabel;
        private System.Windows.Forms.GroupBox addNewMemberBox;
        private System.Windows.Forms.Button CreateMemberButton;
        private System.Windows.Forms.TextBox CellPhoneValue;
        private System.Windows.Forms.Label CellPhoneLabel;
        private System.Windows.Forms.TextBox EmailValue;
        private System.Windows.Forms.Label EmailLabel;
        private System.Windows.Forms.TextBox LastNameValue;
        private System.Windows.Forms.Label LastNameLabel;
        private System.Windows.Forms.TextBox FrstNameValue;
        private System.Windows.Forms.Label FrstNameLabel;
        private System.Windows.Forms.ListBox TeamMembersListBox;
        private System.Windows.Forms.Button DeleteSelectedTeamMemberButton;
        private System.Windows.Forms.Button CreateTeamButton;
    }
}
﻿
namespace TracerUI
{
    partial class TournamentViwerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TournamentViwerForm));
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.TurnamentName = new System.Windows.Forms.Label();
            this.TeamOneName = new System.Windows.Forms.Label();
            this.TeamTwoName = new System.Windows.Forms.Label();
            this.TeamOneScore = new System.Windows.Forms.Label();
            this.TeamTwoScore = new System.Windows.Forms.Label();
            this.TeamOneScoreValue = new System.Windows.Forms.TextBox();
            this.TeamTwoScoreValue = new System.Windows.Forms.TextBox();
            this.Versuslabel = new System.Windows.Forms.Label();
            this.ScoreButton = new System.Windows.Forms.Button();
            this.HeaderLabel = new System.Windows.Forms.Label();
            this.RoundLabel = new System.Windows.Forms.Label();
            this.RoundDropDown = new System.Windows.Forms.ComboBox();
            this.UnplayedOnlyCheckBox1 = new System.Windows.Forms.CheckBox();
            this.MatchUpListBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // TurnamentName
            // 
            resources.ApplyResources(this.TurnamentName, "TurnamentName");
            this.TurnamentName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.TurnamentName.Name = "TurnamentName";
            this.helpProvider1.SetShowHelp(this.TurnamentName, ((bool)(resources.GetObject("TurnamentName.ShowHelp"))));
            this.TurnamentName.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // TeamOneName
            // 
            resources.ApplyResources(this.TeamOneName, "TeamOneName");
            this.TeamOneName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.TeamOneName.Name = "TeamOneName";
            this.helpProvider1.SetShowHelp(this.TeamOneName, ((bool)(resources.GetObject("TeamOneName.ShowHelp"))));
            this.TeamOneName.Click += new System.EventHandler(this.label1_Click_3);
            // 
            // TeamTwoName
            // 
            resources.ApplyResources(this.TeamTwoName, "TeamTwoName");
            this.TeamTwoName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.TeamTwoName.Name = "TeamTwoName";
            this.helpProvider1.SetShowHelp(this.TeamTwoName, ((bool)(resources.GetObject("TeamTwoName.ShowHelp"))));
            this.TeamTwoName.Click += new System.EventHandler(this.TeamTwoName_Click);
            // 
            // TeamOneScore
            // 
            resources.ApplyResources(this.TeamOneScore, "TeamOneScore");
            this.TeamOneScore.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.TeamOneScore.Name = "TeamOneScore";
            this.helpProvider1.SetShowHelp(this.TeamOneScore, ((bool)(resources.GetObject("TeamOneScore.ShowHelp"))));
            // 
            // TeamTwoScore
            // 
            resources.ApplyResources(this.TeamTwoScore, "TeamTwoScore");
            this.TeamTwoScore.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.TeamTwoScore.Name = "TeamTwoScore";
            this.helpProvider1.SetShowHelp(this.TeamTwoScore, ((bool)(resources.GetObject("TeamTwoScore.ShowHelp"))));
            this.TeamTwoScore.Click += new System.EventHandler(this.TeamTwoScore_Click);
            // 
            // TeamOneScoreValue
            // 
            this.TeamOneScoreValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TeamOneScoreValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(151)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.TeamOneScoreValue, "TeamOneScoreValue");
            this.TeamOneScoreValue.Name = "TeamOneScoreValue";
            this.helpProvider1.SetShowHelp(this.TeamOneScoreValue, ((bool)(resources.GetObject("TeamOneScoreValue.ShowHelp"))));
            this.TeamOneScoreValue.TextChanged += new System.EventHandler(this.TeamOneScoreValue_TextChanged);
            // 
            // TeamTwoScoreValue
            // 
            this.TeamTwoScoreValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TeamTwoScoreValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(151)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.TeamTwoScoreValue, "TeamTwoScoreValue");
            this.TeamTwoScoreValue.Name = "TeamTwoScoreValue";
            this.helpProvider1.SetShowHelp(this.TeamTwoScoreValue, ((bool)(resources.GetObject("TeamTwoScoreValue.ShowHelp"))));
            // 
            // Versuslabel
            // 
            resources.ApplyResources(this.Versuslabel, "Versuslabel");
            this.Versuslabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.Versuslabel.Name = "Versuslabel";
            this.helpProvider1.SetShowHelp(this.Versuslabel, ((bool)(resources.GetObject("Versuslabel.ShowHelp"))));
            // 
            // ScoreButton
            // 
            this.ScoreButton.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.ScoreButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.ScoreButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            resources.ApplyResources(this.ScoreButton, "ScoreButton");
            this.ScoreButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.ScoreButton.Name = "ScoreButton";
            this.helpProvider1.SetShowHelp(this.ScoreButton, ((bool)(resources.GetObject("ScoreButton.ShowHelp"))));
            this.ScoreButton.UseVisualStyleBackColor = true;
            // 
            // HeaderLabel
            // 
            resources.ApplyResources(this.HeaderLabel, "HeaderLabel");
            this.HeaderLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.HeaderLabel.Name = "HeaderLabel";
            this.HeaderLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // RoundLabel
            // 
            resources.ApplyResources(this.RoundLabel, "RoundLabel");
            this.RoundLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.RoundLabel.Name = "RoundLabel";
            this.RoundLabel.Click += new System.EventHandler(this.label1_Click_2);
            // 
            // RoundDropDown
            // 
            this.RoundDropDown.FormattingEnabled = true;
            resources.ApplyResources(this.RoundDropDown, "RoundDropDown");
            this.RoundDropDown.Name = "RoundDropDown";
            // 
            // UnplayedOnlyCheckBox1
            // 
            resources.ApplyResources(this.UnplayedOnlyCheckBox1, "UnplayedOnlyCheckBox1");
            this.UnplayedOnlyCheckBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.UnplayedOnlyCheckBox1.Name = "UnplayedOnlyCheckBox1";
            this.UnplayedOnlyCheckBox1.UseVisualStyleBackColor = true;
            this.UnplayedOnlyCheckBox1.CheckedChanged += new System.EventHandler(this.UnplayedOnlyCheckBox1_CheckedChanged);
            // 
            // MatchUpListBox
            // 
            this.MatchUpListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MatchUpListBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(151)))), ((int)(((byte)(255)))));
            this.MatchUpListBox.FormattingEnabled = true;
            resources.ApplyResources(this.MatchUpListBox, "MatchUpListBox");
            this.MatchUpListBox.Name = "MatchUpListBox";
            this.MatchUpListBox.SelectedIndexChanged += new System.EventHandler(this.MatchUpListBox_SelectedIndexChanged);
            // 
            // TournamentViwerForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.ScoreButton);
            this.Controls.Add(this.Versuslabel);
            this.Controls.Add(this.TeamTwoScoreValue);
            this.Controls.Add(this.TeamOneScoreValue);
            this.Controls.Add(this.TeamTwoScore);
            this.Controls.Add(this.TeamOneScore);
            this.Controls.Add(this.TeamTwoName);
            this.Controls.Add(this.TeamOneName);
            this.Controls.Add(this.MatchUpListBox);
            this.Controls.Add(this.UnplayedOnlyCheckBox1);
            this.Controls.Add(this.RoundDropDown);
            this.Controls.Add(this.RoundLabel);
            this.Controls.Add(this.TurnamentName);
            this.Controls.Add(this.HeaderLabel);
            this.Name = "TournamentViwerForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.Label HeaderLabel;
        private System.Windows.Forms.Label TurnamentName;
        private System.Windows.Forms.Label RoundLabel;
        private System.Windows.Forms.ComboBox RoundDropDown;
        private System.Windows.Forms.CheckBox UnplayedOnlyCheckBox1;
        private System.Windows.Forms.ListBox MatchUpListBox;
        private System.Windows.Forms.Label TeamOneName;
        private System.Windows.Forms.Label TeamTwoName;
        private System.Windows.Forms.Label TeamOneScore;
        private System.Windows.Forms.Label TeamTwoScore;
        private System.Windows.Forms.TextBox TeamOneScoreValue;
        private System.Windows.Forms.TextBox TeamTwoScoreValue;
        private System.Windows.Forms.Label Versuslabel;
        private System.Windows.Forms.Button ScoreButton;
    }
}


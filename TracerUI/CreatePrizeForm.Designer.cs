﻿
namespace TracerUI
{
    partial class CreatePrizeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CreatePrizeLabel = new System.Windows.Forms.Label();
            this.PlaceNumberValue = new System.Windows.Forms.TextBox();
            this.PlaceNumberLabel = new System.Windows.Forms.Label();
            this.PrizeAmountValue = new System.Windows.Forms.TextBox();
            this.PrizeAmountLabel = new System.Windows.Forms.Label();
            this.PlaceNameValue = new System.Windows.Forms.TextBox();
            this.PlaceName = new System.Windows.Forms.Label();
            this.OrLabel = new System.Windows.Forms.Label();
            this.PrizePercentageValue = new System.Windows.Forms.TextBox();
            this.PrizePercentageLabel = new System.Windows.Forms.Label();
            this.CreatePrizeButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CreatePrizeLabel
            // 
            this.CreatePrizeLabel.AutoSize = true;
            this.CreatePrizeLabel.Font = new System.Drawing.Font("Segoe UI Light", 22F);
            this.CreatePrizeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.CreatePrizeLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.CreatePrizeLabel.Location = new System.Drawing.Point(130, 9);
            this.CreatePrizeLabel.Name = "CreatePrizeLabel";
            this.CreatePrizeLabel.Size = new System.Drawing.Size(247, 60);
            this.CreatePrizeLabel.TabIndex = 27;
            this.CreatePrizeLabel.Text = "Create Prize";
            // 
            // PlaceNumberValue
            // 
            this.PlaceNumberValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PlaceNumberValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(151)))), ((int)(((byte)(255)))));
            this.PlaceNumberValue.Location = new System.Drawing.Point(273, 79);
            this.PlaceNumberValue.Name = "PlaceNumberValue";
            this.PlaceNumberValue.Size = new System.Drawing.Size(280, 61);
            this.PlaceNumberValue.TabIndex = 29;
            // 
            // PlaceNumberLabel
            // 
            this.PlaceNumberLabel.AutoSize = true;
            this.PlaceNumberLabel.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.PlaceNumberLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.PlaceNumberLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.PlaceNumberLabel.Location = new System.Drawing.Point(12, 79);
            this.PlaceNumberLabel.Name = "PlaceNumberLabel";
            this.PlaceNumberLabel.Size = new System.Drawing.Size(214, 45);
            this.PlaceNumberLabel.TabIndex = 28;
            this.PlaceNumberLabel.Text = "Place number";
            this.PlaceNumberLabel.Click += new System.EventHandler(this.FrstNameLabel_Click);
            // 
            // PrizeAmountValue
            // 
            this.PrizeAmountValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PrizeAmountValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(151)))), ((int)(((byte)(255)))));
            this.PrizeAmountValue.Location = new System.Drawing.Point(273, 213);
            this.PrizeAmountValue.Name = "PrizeAmountValue";
            this.PrizeAmountValue.Size = new System.Drawing.Size(280, 61);
            this.PrizeAmountValue.TabIndex = 31;
            this.PrizeAmountValue.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // PrizeAmountLabel
            // 
            this.PrizeAmountLabel.AutoSize = true;
            this.PrizeAmountLabel.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.PrizeAmountLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.PrizeAmountLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.PrizeAmountLabel.Location = new System.Drawing.Point(12, 213);
            this.PrizeAmountLabel.Name = "PrizeAmountLabel";
            this.PrizeAmountLabel.Size = new System.Drawing.Size(189, 45);
            this.PrizeAmountLabel.TabIndex = 30;
            this.PrizeAmountLabel.Text = "Prize amont";
            // 
            // PlaceNameValue
            // 
            this.PlaceNameValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PlaceNameValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(151)))), ((int)(((byte)(255)))));
            this.PlaceNameValue.Location = new System.Drawing.Point(273, 146);
            this.PlaceNameValue.Name = "PlaceNameValue";
            this.PlaceNameValue.Size = new System.Drawing.Size(280, 61);
            this.PlaceNameValue.TabIndex = 33;
            // 
            // PlaceName
            // 
            this.PlaceName.AutoSize = true;
            this.PlaceName.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.PlaceName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.PlaceName.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.PlaceName.Location = new System.Drawing.Point(12, 146);
            this.PlaceName.Name = "PlaceName";
            this.PlaceName.Size = new System.Drawing.Size(182, 45);
            this.PlaceName.TabIndex = 32;
            this.PlaceName.Text = "Place name";
            // 
            // OrLabel
            // 
            this.OrLabel.AutoSize = true;
            this.OrLabel.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.OrLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.OrLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.OrLabel.Location = new System.Drawing.Point(218, 323);
            this.OrLabel.Name = "OrLabel";
            this.OrLabel.Size = new System.Drawing.Size(76, 45);
            this.OrLabel.TabIndex = 34;
            this.OrLabel.Text = "-or-";
            this.OrLabel.Click += new System.EventHandler(this.OrLabel_Click);
            // 
            // PrizePercentageValue
            // 
            this.PrizePercentageValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PrizePercentageValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(151)))), ((int)(((byte)(255)))));
            this.PrizePercentageValue.Location = new System.Drawing.Point(273, 406);
            this.PrizePercentageValue.Name = "PrizePercentageValue";
            this.PrizePercentageValue.Size = new System.Drawing.Size(280, 61);
            this.PrizePercentageValue.TabIndex = 36;
            // 
            // PrizePercentageLabel
            // 
            this.PrizePercentageLabel.AutoSize = true;
            this.PrizePercentageLabel.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.PrizePercentageLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.PrizePercentageLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.PrizePercentageLabel.Location = new System.Drawing.Point(12, 406);
            this.PrizePercentageLabel.Name = "PrizePercentageLabel";
            this.PrizePercentageLabel.Size = new System.Drawing.Size(255, 45);
            this.PrizePercentageLabel.TabIndex = 35;
            this.PrizePercentageLabel.Text = "Prize Percentage";
            // 
            // CreatePrizeButton
            // 
            this.CreatePrizeButton.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.CreatePrizeButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.CreatePrizeButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.CreatePrizeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreatePrizeButton.Font = new System.Drawing.Font("Segoe UI Semibold", 16F, System.Drawing.FontStyle.Bold);
            this.CreatePrizeButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.CreatePrizeButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.CreatePrizeButton.Location = new System.Drawing.Point(140, 528);
            this.CreatePrizeButton.Name = "CreatePrizeButton";
            this.CreatePrizeButton.Size = new System.Drawing.Size(346, 57);
            this.CreatePrizeButton.TabIndex = 38;
            this.CreatePrizeButton.Text = "Create  Prize";
            this.CreatePrizeButton.UseVisualStyleBackColor = true;
            this.CreatePrizeButton.Click += new System.EventHandler(this.CreatePrizeButton_Click);
            // 
            // CreatePrizeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(22F, 54F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(698, 609);
            this.Controls.Add(this.CreatePrizeButton);
            this.Controls.Add(this.PrizePercentageValue);
            this.Controls.Add(this.PrizePercentageLabel);
            this.Controls.Add(this.OrLabel);
            this.Controls.Add(this.PlaceNameValue);
            this.Controls.Add(this.PlaceName);
            this.Controls.Add(this.PrizeAmountValue);
            this.Controls.Add(this.PrizeAmountLabel);
            this.Controls.Add(this.PlaceNumberValue);
            this.Controls.Add(this.PlaceNumberLabel);
            this.Controls.Add(this.CreatePrizeLabel);
            this.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.Name = "CreatePrizeForm";
            this.Text = "Create Prize";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label CreatePrizeLabel;
        private System.Windows.Forms.TextBox PlaceNumberValue;
        private System.Windows.Forms.TextBox PrizeAmountValue;
        private System.Windows.Forms.Label PrizeAmountLabel;
        private System.Windows.Forms.TextBox PlaceNameValue;
        private System.Windows.Forms.Label PlaceName;
        private System.Windows.Forms.Label OrLabel;
        private System.Windows.Forms.TextBox PrizePercentageValue;
        private System.Windows.Forms.Label PrizePercentageLabel;
        private System.Windows.Forms.Button CreatePrizeButton;
        private System.Windows.Forms.Label PlaceNumberLabel;
    }
}
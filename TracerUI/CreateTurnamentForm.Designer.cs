﻿
namespace TracerUI
{
    partial class CreateTurnamentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HeaderLabel = new System.Windows.Forms.Label();
            this.TournamentNameValue = new System.Windows.Forms.TextBox();
            this.TurnamentPlayersLabel = new System.Windows.Forms.Label();
            this.EntryFeeValue = new System.Windows.Forms.TextBox();
            this.EntryFeeLabel = new System.Windows.Forms.Label();
            this.SelectTeamDropDown = new System.Windows.Forms.ComboBox();
            this.SelectTeamLabel = new System.Windows.Forms.Label();
            this.CreatNewTeamLinkLabel = new System.Windows.Forms.LinkLabel();
            this.AddTeamButton = new System.Windows.Forms.Button();
            this.CreateTurnamentButton = new System.Windows.Forms.Button();
            this.TurnamentPlayersListBox = new System.Windows.Forms.ListBox();
            this.DeleteSelectedPlayerButton = new System.Windows.Forms.Button();
            this.DeleteSelectedPrizeButton = new System.Windows.Forms.Button();
            this.PrizesListBox = new System.Windows.Forms.ListBox();
            this.PrizesLabel = new System.Windows.Forms.Label();
            this.TurnamentnameLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.AutoSize = true;
            this.HeaderLabel.Font = new System.Drawing.Font("Segoe UI Light", 22F);
            this.HeaderLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.HeaderLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.HeaderLabel.Location = new System.Drawing.Point(16, 21);
            this.HeaderLabel.Name = "HeaderLabel";
            this.HeaderLabel.Size = new System.Drawing.Size(363, 60);
            this.HeaderLabel.TabIndex = 1;
            this.HeaderLabel.Text = "Create Turnament:";
            // 
            // TournamentNameValue
            // 
            this.TournamentNameValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TournamentNameValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(151)))), ((int)(((byte)(255)))));
            this.TournamentNameValue.Location = new System.Drawing.Point(38, 142);
            this.TournamentNameValue.Name = "TournamentNameValue";
            this.TournamentNameValue.Size = new System.Drawing.Size(359, 66);
            this.TournamentNameValue.TabIndex = 12;
            // 
            // TurnamentPlayersLabel
            // 
            this.TurnamentPlayersLabel.AutoSize = true;
            this.TurnamentPlayersLabel.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.TurnamentPlayersLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.TurnamentPlayersLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.TurnamentPlayersLabel.Location = new System.Drawing.Point(444, 94);
            this.TurnamentPlayersLabel.Name = "TurnamentPlayersLabel";
            this.TurnamentPlayersLabel.Size = new System.Drawing.Size(220, 45);
            this.TurnamentPlayersLabel.TabIndex = 11;
            this.TurnamentPlayersLabel.Text = "Teams/Players";
            this.TurnamentPlayersLabel.Click += new System.EventHandler(this.TeamOneScore_Click);
            // 
            // EntryFeeValue
            // 
            this.EntryFeeValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.EntryFeeValue.ForeColor = System.Drawing.Color.Black;
            this.EntryFeeValue.Location = new System.Drawing.Point(234, 211);
            this.EntryFeeValue.Name = "EntryFeeValue";
            this.EntryFeeValue.Size = new System.Drawing.Size(163, 66);
            this.EntryFeeValue.TabIndex = 14;
            this.EntryFeeValue.Text = "0";
            this.EntryFeeValue.TextChanged += new System.EventHandler(this.TeamOneScoreValue_TextChanged);
            // 
            // EntryFeeLabel
            // 
            this.EntryFeeLabel.AutoSize = true;
            this.EntryFeeLabel.Font = new System.Drawing.Font("Segoe UI", 20F);
            this.EntryFeeLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.EntryFeeLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.EntryFeeLabel.Location = new System.Drawing.Point(42, 211);
            this.EntryFeeLabel.Name = "EntryFeeLabel";
            this.EntryFeeLabel.Size = new System.Drawing.Size(186, 54);
            this.EntryFeeLabel.TabIndex = 13;
            this.EntryFeeLabel.Text = "Entry Fee";
            // 
            // SelectTeamDropDown
            // 
            this.SelectTeamDropDown.FormattingEnabled = true;
            this.SelectTeamDropDown.Location = new System.Drawing.Point(38, 357);
            this.SelectTeamDropDown.Name = "SelectTeamDropDown";
            this.SelectTeamDropDown.Size = new System.Drawing.Size(359, 68);
            this.SelectTeamDropDown.TabIndex = 16;
            // 
            // SelectTeamLabel
            // 
            this.SelectTeamLabel.AutoSize = true;
            this.SelectTeamLabel.Font = new System.Drawing.Font("Segoe UI", 20F);
            this.SelectTeamLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.SelectTeamLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SelectTeamLabel.Location = new System.Drawing.Point(42, 300);
            this.SelectTeamLabel.Name = "SelectTeamLabel";
            this.SelectTeamLabel.Size = new System.Drawing.Size(231, 54);
            this.SelectTeamLabel.TabIndex = 15;
            this.SelectTeamLabel.Text = "Select Team";
            // 
            // CreatNewTeamLinkLabel
            // 
            this.CreatNewTeamLinkLabel.AutoSize = true;
            this.CreatNewTeamLinkLabel.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreatNewTeamLinkLabel.Location = new System.Drawing.Point(266, 317);
            this.CreatNewTeamLinkLabel.Name = "CreatNewTeamLinkLabel";
            this.CreatNewTeamLinkLabel.Size = new System.Drawing.Size(113, 28);
            this.CreatNewTeamLinkLabel.TabIndex = 17;
            this.CreatNewTeamLinkLabel.TabStop = true;
            this.CreatNewTeamLinkLabel.Text = "Create new ";
            // 
            // AddTeamButton
            // 
            this.AddTeamButton.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.AddTeamButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.AddTeamButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.AddTeamButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddTeamButton.Font = new System.Drawing.Font("Segoe UI Semibold", 16F, System.Drawing.FontStyle.Bold);
            this.AddTeamButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.AddTeamButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.AddTeamButton.Location = new System.Drawing.Point(38, 431);
            this.AddTeamButton.Name = "AddTeamButton";
            this.AddTeamButton.Size = new System.Drawing.Size(222, 57);
            this.AddTeamButton.TabIndex = 18;
            this.AddTeamButton.Text = "Add Team";
            this.AddTeamButton.UseVisualStyleBackColor = true;
            // 
            // CreateTurnamentButton
            // 
            this.CreateTurnamentButton.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.CreateTurnamentButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.CreateTurnamentButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.CreateTurnamentButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateTurnamentButton.Font = new System.Drawing.Font("Segoe UI Semibold", 16F, System.Drawing.FontStyle.Bold);
            this.CreateTurnamentButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.CreateTurnamentButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.CreateTurnamentButton.Location = new System.Drawing.Point(260, 604);
            this.CreateTurnamentButton.Name = "CreateTurnamentButton";
            this.CreateTurnamentButton.Size = new System.Drawing.Size(346, 57);
            this.CreateTurnamentButton.TabIndex = 19;
            this.CreateTurnamentButton.Text = "Create Turnament";
            this.CreateTurnamentButton.UseVisualStyleBackColor = true;
            this.CreateTurnamentButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // TurnamentPlayersListBox
            // 
            this.TurnamentPlayersListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TurnamentPlayersListBox.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.TurnamentPlayersListBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(151)))), ((int)(((byte)(255)))));
            this.TurnamentPlayersListBox.FormattingEnabled = true;
            this.TurnamentPlayersListBox.ItemHeight = 32;
            this.TurnamentPlayersListBox.Location = new System.Drawing.Point(452, 142);
            this.TurnamentPlayersListBox.Name = "TurnamentPlayersListBox";
            this.TurnamentPlayersListBox.Size = new System.Drawing.Size(318, 194);
            this.TurnamentPlayersListBox.TabIndex = 20;
            // 
            // DeleteSelectedPlayerButton
            // 
            this.DeleteSelectedPlayerButton.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.DeleteSelectedPlayerButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.DeleteSelectedPlayerButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.DeleteSelectedPlayerButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteSelectedPlayerButton.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.DeleteSelectedPlayerButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.DeleteSelectedPlayerButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.DeleteSelectedPlayerButton.Location = new System.Drawing.Point(776, 188);
            this.DeleteSelectedPlayerButton.Name = "DeleteSelectedPlayerButton";
            this.DeleteSelectedPlayerButton.Size = new System.Drawing.Size(147, 77);
            this.DeleteSelectedPlayerButton.TabIndex = 21;
            this.DeleteSelectedPlayerButton.Text = "Delete Selected";
            this.DeleteSelectedPlayerButton.UseVisualStyleBackColor = true;
            // 
            // DeleteSelectedPrizeButton
            // 
            this.DeleteSelectedPrizeButton.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.DeleteSelectedPrizeButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.DeleteSelectedPrizeButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.DeleteSelectedPrizeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteSelectedPrizeButton.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.DeleteSelectedPrizeButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.DeleteSelectedPrizeButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.DeleteSelectedPrizeButton.Location = new System.Drawing.Point(776, 450);
            this.DeleteSelectedPrizeButton.Name = "DeleteSelectedPrizeButton";
            this.DeleteSelectedPrizeButton.Size = new System.Drawing.Size(147, 77);
            this.DeleteSelectedPrizeButton.TabIndex = 24;
            this.DeleteSelectedPrizeButton.Text = "Delete Selected";
            this.DeleteSelectedPrizeButton.UseVisualStyleBackColor = true;
            this.DeleteSelectedPrizeButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // PrizesListBox
            // 
            this.PrizesListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PrizesListBox.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.PrizesListBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(151)))), ((int)(((byte)(255)))));
            this.PrizesListBox.FormattingEnabled = true;
            this.PrizesListBox.ItemHeight = 32;
            this.PrizesListBox.Location = new System.Drawing.Point(452, 404);
            this.PrizesListBox.Name = "PrizesListBox";
            this.PrizesListBox.Size = new System.Drawing.Size(318, 194);
            this.PrizesListBox.TabIndex = 23;
            this.PrizesListBox.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // PrizesLabel
            // 
            this.PrizesLabel.AutoSize = true;
            this.PrizesLabel.Font = new System.Drawing.Font("Segoe UI", 16F);
            this.PrizesLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.PrizesLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.PrizesLabel.Location = new System.Drawing.Point(444, 356);
            this.PrizesLabel.Name = "PrizesLabel";
            this.PrizesLabel.Size = new System.Drawing.Size(102, 45);
            this.PrizesLabel.TabIndex = 22;
            this.PrizesLabel.Text = "Prizes";
            this.PrizesLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // TurnamentnameLabel
            // 
            this.TurnamentnameLabel.AutoSize = true;
            this.TurnamentnameLabel.Font = new System.Drawing.Font("Segoe UI", 20F);
            this.TurnamentnameLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.TurnamentnameLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.TurnamentnameLabel.Location = new System.Drawing.Point(42, 85);
            this.TurnamentnameLabel.Name = "TurnamentnameLabel";
            this.TurnamentnameLabel.Size = new System.Drawing.Size(332, 54);
            this.TurnamentnameLabel.TabIndex = 25;
            this.TurnamentnameLabel.Text = "Turnament Name";
            // 
            // CreateTurnamentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(24F, 60F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(925, 671);
            this.Controls.Add(this.TurnamentnameLabel);
            this.Controls.Add(this.DeleteSelectedPrizeButton);
            this.Controls.Add(this.PrizesListBox);
            this.Controls.Add(this.PrizesLabel);
            this.Controls.Add(this.DeleteSelectedPlayerButton);
            this.Controls.Add(this.TurnamentPlayersListBox);
            this.Controls.Add(this.CreateTurnamentButton);
            this.Controls.Add(this.AddTeamButton);
            this.Controls.Add(this.CreatNewTeamLinkLabel);
            this.Controls.Add(this.SelectTeamDropDown);
            this.Controls.Add(this.SelectTeamLabel);
            this.Controls.Add(this.EntryFeeValue);
            this.Controls.Add(this.EntryFeeLabel);
            this.Controls.Add(this.TournamentNameValue);
            this.Controls.Add(this.TurnamentPlayersLabel);
            this.Controls.Add(this.HeaderLabel);
            this.Font = new System.Drawing.Font("Segoe UI", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.Name = "CreateTurnamentForm";
            this.Text = "Create Tournament ";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HeaderLabel;
        private System.Windows.Forms.TextBox TournamentNameValue;
        private System.Windows.Forms.Label TurnamentPlayersLabel;
        private System.Windows.Forms.TextBox EntryFeeValue;
        private System.Windows.Forms.Label EntryFeeLabel;
        private System.Windows.Forms.ComboBox SelectTeamDropDown;
        private System.Windows.Forms.Label SelectTeamLabel;
        private System.Windows.Forms.LinkLabel CreatNewTeamLinkLabel;
        private System.Windows.Forms.Button AddTeamButton;
        private System.Windows.Forms.Button CreateTurnamentButton;
        private System.Windows.Forms.ListBox TurnamentPlayersListBox;
        private System.Windows.Forms.Button DeleteSelectedPlayerButton;
        private System.Windows.Forms.Button DeleteSelectedPrizeButton;
        private System.Windows.Forms.ListBox PrizesListBox;
        private System.Windows.Forms.Label PrizesLabel;
        private System.Windows.Forms.Label TurnamentnameLabel;
    }
}
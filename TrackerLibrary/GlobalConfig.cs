﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackerLibrary
{
   public static class GlobalConfig
    {
        public static List<IDataConnection> Connections { get; private set; } = new List<IDataConnection>();

        public static void InitConections(bool DataBase , bool TextFiles)
        {
            if(DataBase)
            {

                SqlConnector sql = new SqlConnector() ;
                Connections.Add(sql);
            }

            if(TextFiles)
            {

                TextConnection txt = new TextConnection();
                Connections.Add(txt);

            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackerLibrary
{
    public class PrizeModel
    {

        /// <summary>
        ///  The unique Indentifier for the prize.
        /// </summary>
        public int Id { get; set; }
        public int PlaceNumber;
        public string PlaceName;
        public decimal PrizeAmount;
        public double PrizePercentage;

    public PrizeModel()
        {

        }

        public PrizeModel(string placeNumber,string placeName,string prizeAmount,string  prizePercentage)
        {
            placeName = PlaceName;
            int placeNumberValue = 0;

            int.TryParse(placeNumber, out placeNumberValue);
            PlaceNumber = placeNumberValue;

            decimal prizeAmountValue = 0;
            decimal.TryParse(prizeAmount, out prizeAmountValue);
            PrizeAmount = prizeAmountValue;

            double prizePercentageValue = 0;

            double.TryParse(prizePercentage, out prizePercentageValue);

            PrizePercentage = prizePercentageValue;


        }
    }
}

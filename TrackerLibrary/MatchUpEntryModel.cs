﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackerLibrary
{
    public class MatchUpEntryModel
    {
        public TeamModel TeamCompiting { get; set; }
        public double Score;
        public MatchUpModel ParentMatchUp;
    }
}
